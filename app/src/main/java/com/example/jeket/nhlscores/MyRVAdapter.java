package com.example.jeket.nhlscores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MyRVAdapter extends RecyclerView.Adapter<MyRVAdapter.MyViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(GameItem gameItem);
    }

    private final OnItemClickListener listener;
    private ArrayList<GameItem> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView homeTeamTV;
        public TextView awayTeamTV;
        public TextView homeScoreTV;
        public TextView awayScoreTV;

        public MyViewHolder(View v) {
            super(v);
            homeTeamTV = v.findViewById(R.id.home_team);
            awayTeamTV = v.findViewById(R.id.away_team);
            homeScoreTV = v.findViewById(R.id.home_score);
            awayScoreTV = v.findViewById(R.id.away_score);
        }

        //Click listener
        public void bind(final GameItem gameItem, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(gameItem);
                }
            });
        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyRVAdapter(ArrayList<GameItem> myDataset, OnItemClickListener listener) {
        mDataset = myDataset;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyRVAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        // create a new view
        TextView testTextView = new TextView(context);
        testTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        View v = inflater.inflate(R.layout.list_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.homeTeamTV.setText(mDataset.get(position).getHomeTeam());
        holder.awayTeamTV.setText(mDataset.get(position).getAwayTeam());
        holder.homeScoreTV.setText(String.valueOf(mDataset.get(position).getHomeScore()));
        holder.awayScoreTV.setText(String.valueOf(mDataset.get(position).getAwayScore()));

        //click listener
        holder.bind(mDataset.get(position), listener);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }



}
