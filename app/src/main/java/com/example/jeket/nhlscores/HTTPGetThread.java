package com.example.jeket.nhlscores;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPGetThread extends Thread {
    //Interface
    protected OnRequestDoneInterface listener = null;

    public interface OnRequestDoneInterface {
        void onRequestDone(String data);
    }


    private String urlString = "";
    public HTTPGetThread(String url, OnRequestDoneInterface listener) {
        urlString = url;
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            getMenu();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Load menu from url
    private void getMenu() {
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String allData = fromStream(in);
            Log.d("THREAD-1",allData);
            listener.onRequestDone(allData);
            in.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if (urlConnection != null)
            {
                urlConnection.disconnect();
            }
        }
    }

    private static String fromStream(InputStream in) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder out = new StringBuilder();
        String newLine = System.getProperty("line.separator");
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
            out.append(newLine);
        }
        return out.toString();
    }

}
