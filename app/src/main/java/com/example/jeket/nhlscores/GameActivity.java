package com.example.jeket.nhlscores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class GameActivity extends AppCompatActivity implements JSONParser.JSONInterface {

    TextView goalDescTv;
    TextView homeTv;
    TextView awayTv;
    TextView scoreTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        goalDescTv = findViewById(R.id.goal_info);
        homeTv = findViewById(R.id.home_team_tv);
        awayTv = findViewById(R.id.away_team_tv);
        scoreTv = findViewById(R.id.score_tv);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        GameItem gameItem = (GameItem) bundle.getSerializable(MainActivity.EXTRA_MESSAGE);
        String gamePk = String.valueOf(gameItem.getGamePk());
        Log.d("JEKETTU", String.valueOf(gamePk));


        String url = "https://statsapi.web.nhl.com/api/v1/game/" + gamePk + "/feed/live";
        JSONParser thread = new JSONParser(url, this, 1);
        thread.start();

        String score = gameItem.getHomeScore() + " - " +gameItem.getAwayScore();
        scoreTv.setText(score);
    }

    @Override
    public void onMenuItemDone(ArrayList<GameItem> menuArrayList) {

    }

    @Override
    public void onScoreItemDone(final GameItem gameItem) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < gameItem.getScorersList().size(); i++) {
                    String scorer = gameItem.getScorersList().get(i).getScorer();
                    String periodTime = gameItem.getScorersList().get(i).getTime();
                    String period = gameItem.getScorersList().get(i).getPeriod();
                    String team = gameItem.getScorersList().get(i).getTeam();

                    String goalInfo = period + " - " + periodTime + " - (" + team + "):" +scorer + "\n\n";
                    goalDescTv.append(goalInfo);
                    goalDescTv.setMovementMethod(new ScrollingMovementMethod());
                    homeTv.setText(gameItem.getHomeTeam());
                    awayTv.setText(gameItem.getAwayTeam());
                }

            }
        });
    }
}
