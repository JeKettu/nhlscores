package com.example.jeket.nhlscores;

import java.io.Serializable;
import java.util.ArrayList;

public class GameItem implements Serializable {
    private int gamePk;
    private String homeTeam;
    private String awayTeam;
    private int homeScore;
    private int awayScore;
    private ArrayList<ScoreInfo> scorersList;
    private ScoreInfo scoreInfo;

    public GameItem() {
        scorersList = new ArrayList<>();
    }

    public GameItem(int gamePk, String homeTeam, String awayTeam, int homeScore, int awayScore) {
        this.gamePk = gamePk;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        scorersList = new ArrayList<>();
    }

    public int getGamePk() {
        return gamePk;
    }

    public void setGamePk(int gamePk) {
        this.gamePk = gamePk;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }

    public ArrayList<ScoreInfo> getScorersList() {
        return scorersList;
    }

    public void setScorersList(ArrayList<ScoreInfo> scorersList) {
        this.scorersList = scorersList;
    }

    public  void addScoreInfo(String scorer, String period, String time, String team) {
        scoreInfo = new ScoreInfo(scorer,period,time, team);
        scorersList.add(scoreInfo);
    }


    public class ScoreInfo {
        private String scorer;
        private String period;
        private String time;
        private String team;

        public ScoreInfo(String scorer, String period, String time, String team) {
            this.scorer = scorer;
            this.period = period;
            this.time = time;
            this.team = team;
        }

        public String getScorer() {
            return scorer;
        }

        public void setScorer(String scorer) {
            this.scorer = scorer;
        }

        public String getPeriod() {
            return period;
        }

        public void setPeriod(String period) {
            this.period = period;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTeam() {
            return team;
        }

        public void setTeam(String team) {
            this.team = team;
        }
    }
}
