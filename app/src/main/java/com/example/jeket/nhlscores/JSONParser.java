package com.example.jeket.nhlscores;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONParser extends Thread implements HTTPGetThread.OnRequestDoneInterface {

    protected JSONInterface listener = null;
    private String url = "";
    private int activityId;

    public JSONParser(String url, JSONInterface listener, int activityId) {
        this.url = url;
        this.listener = listener;
        this.activityId = activityId;

    }

    public interface JSONInterface{
        void onMenuItemDone(ArrayList<GameItem> menuArrayList);
        void onScoreItemDone(GameItem gameItem);
    }

    @Override
    public void run() {
        try {
            loadJson();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    //load menu from server
    private void loadJson(){
        HTTPGetThread thread = new HTTPGetThread(url, this);
        thread.start();
    }


    @Override
    public void onRequestDone(String data) {
        if (activityId == 0) {
            parseAllGameData(data);
        } else if (activityId == 1) {
           parseSingleGameData(data);
        }


    }

    private void parseAllGameData(String data){
        //Create list of MenuItem-objects
        ArrayList<GameItem> gameArrayList = new ArrayList<>();
        if (data != null) {
            try{
                JSONObject allJObj = new JSONObject(data);
                JSONArray datesJArr = allJObj.getJSONArray("dates");
                JSONObject gamesJObj = datesJArr.getJSONObject(0);
                JSONArray gamesArr = gamesJObj.getJSONArray("games");

                for (int gameId = 0; gameId < gamesArr.length(); gameId++){
                    JSONObject gameJObj = gamesArr.getJSONObject(gameId);
                    JSONObject teamsJObj = gameJObj.getJSONObject("teams");
                    JSONObject awayJObj = teamsJObj.getJSONObject("away");
                    JSONObject awayTeamJObj = awayJObj.getJSONObject("team");
                    JSONObject homeJObj = teamsJObj.getJSONObject("home");
                    JSONObject homeTeamJObj = homeJObj.getJSONObject("team");
                    int gamePk = gameJObj.getInt("gamePk");
                    int awayScore = awayJObj.getInt("score");
                    int homeScore = homeJObj.getInt("score");
                    String awayName = awayTeamJObj.getString("name");
                    String homeName = homeTeamJObj.getString("name");
                    Log.d("JEKETTU", String.valueOf(gamePk));

                    gameArrayList.add(new GameItem(gamePk,homeName, awayName, homeScore, awayScore));
                }
            }
            catch (final JSONException e) {
                Log.e("JSON", "Json parsing error: " + e.getMessage());
            }
        }
        listener.onMenuItemDone(gameArrayList);
    }

    private void parseSingleGameData(String data){

        if (data != null) {

            GameItem gameItem = new GameItem();
            try{
                JSONObject allJObj = new JSONObject(data);

                JSONObject gameDataJObj = allJObj.getJSONObject("gameData");
                JSONObject teamsJObj = gameDataJObj.getJSONObject("teams");
                JSONObject awayJObj = teamsJObj.getJSONObject("away");
                JSONObject homeJObj = teamsJObj.getJSONObject("home");
                String home = homeJObj.getString("shortName");
                String away = awayJObj.getString("shortName");
                gameItem.setHomeTeam(home);
                gameItem.setAwayTeam(away);


                JSONObject liveDataJObj = allJObj.getJSONObject("liveData");
                JSONObject playsJObj = liveDataJObj.getJSONObject("plays");
                JSONArray allPlaysArr = playsJObj.getJSONArray("allPlays");
                JSONArray scoringPlaysArr = playsJObj.getJSONArray("scoringPlays");
                for (int i = 0; i < scoringPlaysArr.length(); i++) {
                    int scoringPlayId = scoringPlaysArr.getInt(i);
                    JSONObject allPlaysIdJObj = allPlaysArr.getJSONObject(scoringPlayId);
                    JSONObject resultJObj = allPlaysIdJObj.getJSONObject("result");
                    String goalInfo = resultJObj.getString("description");
                    JSONObject teamJObj = allPlaysIdJObj.getJSONObject("team");
                    String team = teamJObj.getString("triCode");
                    JSONObject aboutJObj = allPlaysIdJObj.getJSONObject("about");
                    String periodTime = aboutJObj.getString("periodTime");
                    String period = aboutJObj.getString("ordinalNum");
                    gameItem.addScoreInfo(goalInfo, period, periodTime, team);
                    Log.d("JEKETTU", goalInfo);
                }


                Log.d("JEKETTU", allJObj.toString());
            }
            catch (final JSONException e) {
                Log.e("JSON", "Json parsing error: " + e.getMessage());
            }
            listener.onScoreItemDone(gameItem);
        }

    }
}

