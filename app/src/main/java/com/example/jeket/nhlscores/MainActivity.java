package com.example.jeket.nhlscores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements JSONParser.JSONInterface {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //find views
        mRecyclerView = findViewById(R.id.my_recycler_view);
        TextView dateTextView = findViewById(R.id.date);

        // use a linear layout manager for recyclerView
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        //Get Date
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24)));
        dateTextView.setText(date);

        //json
        JSONParser thread = new JSONParser("https://statsapi.web.nhl.com/api/v1/schedule?date="+date, this, 0);
        thread.start();


    }


    @Override
    public void onMenuItemDone(final ArrayList<GameItem> menuArrayList) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // specify an adapter (see also next example)
                mAdapter = new MyRVAdapter(menuArrayList, new MyRVAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(GameItem gameItem) {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(EXTRA_MESSAGE,gameItem);
                        Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });
                mRecyclerView.setAdapter(mAdapter);
            }
        });
    }

    @Override
    public void onScoreItemDone(GameItem gameItem) {

    }
}
